package com.ithillel.testing;

import org.junit.*;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

public class JUnitDemo {

    @BeforeClass
    public static void befCl(){
        System.out.println("Before class ...");
    }

    @AfterClass
    public static void aftCl(){
        System.out.println("After class ...");
    }

    @Before
    public void setUp() throws Exception {
        System.out.println("before");
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("after");
    }

    @Test
    public void test1() {
        System.out.println("Test1...");

        Assert.assertEquals("hello", "hello");
        List<String> list = Arrays.asList("h", "ww", "ddd");

        assertThat(list, hasItem("ddd"));

    }

    @Test(expected = RuntimeException.class)
    public void test2() {
        System.out.println("Test2...");
        throw new RuntimeException();
    }

    @Test
    public void test3() {
        System.out.println("Test3...");
        Assert.assertEquals("hello", "hello");
        Assert.fail();
    }



}
